﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RateSheetExcelDataUpdates
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Microsoft.Office.Interop.Excel.Application app = new Microsoft.Office.Interop.Excel.Application();
            Microsoft.Office.Interop.Excel.Workbook wb = app.Workbooks.Open(@"C:\Users\ChadLutz\source\repos\ExcelFiles\[FILE_NAME].xlsx");
            Microsoft.Office.Interop.Excel.Worksheet ws = wb.Worksheets[1];
            Microsoft.Office.Interop.Excel.Range rng = ws.UsedRange;
            int RowCount = rng.Rows.Count;
            int ColCount = rng.Columns.Count;
            clsDBO.clsDBO dBO = new clsDBO.clsDBO();
            string sSQL, CellValue = "", RateSheetID = "72", RateTypeID = "1", PlanTypeID = "", DeductID = "4", EngineLow = "NULL", EngineHigh = "NULL", MileBandLow = "", MileBandHigh = "", 
                AgeMin = "", AgeMax = "", TermMonth = "", TermMile = "", VClass = "", Amt = "", CostLow = "0.00", CostHigh = "0.00";
            string ConnectionString = "server=198.143.98.122;database=veritas;Min Pool Size=5;Max Pool Size=10000;Connect Timeout=60;User Id=sa;Password=NCC1701E";
            bool AddData = true;
            for (int row = 491; row <= 730; row++)
            {
                for (int col = 1; col <= ColCount; col++)
                {
                    Microsoft.Office.Interop.Excel.Range range = ws.Cells[row, col];
                    CellValue = range.Value.ToString();
                    if (col > 6)
                    {
                        if (CellValue.Trim().ToUpper() == "N/A")
                        {
                            AddData = false;
                        }
                        Amt = CellValue.Trim();
                        VClass = ws.Cells[490, col].Value.ToString();
                        if (col >= 7 && col <= 16)
                        {
                            PlanTypeID = "1";
                        }
                        else if (col >= 17 && col <= 26)
                        {
                            PlanTypeID = "2";
                        }
                        else if (col >= 27 && col <= 36)
                        {
                            PlanTypeID = "3";
                        }
                        else if (col >= 37 && col <= 46)
                        {
                            PlanTypeID = "4";
                        }
                        else if (col >= 47 && col <= 56)
                        {
                            PlanTypeID = "5";
                        }
                        else if (col >= 57 && col <= 66)
                        {
                            PlanTypeID = "6";
                        }
                        else if (col >= 67 && col <= 76)
                        {
                            PlanTypeID = "7";
                        }
                        if (AddData)
                        {
                            sSQL = "insert into RateSheetDetail " +
                                   "values (" + RateSheetID + ", " + PlanTypeID + ", " + DeductID + ", " + EngineLow + ", " + EngineHigh + ", " + MileBandLow + ", " + MileBandHigh +
                                   ", " + AgeMin + ", " + AgeMax + ", " + TermMonth + ", " + TermMile + ", " + RateTypeID + 
                                   ", '" + VClass + "', " + Amt + ", " + CostLow + ", " + CostHigh + ") ";

                            dBO.RunSQL(sSQL, ConnectionString);
                            Console.WriteLine(row+"/"+col + " - " + MileBandLow + " " + MileBandHigh);
                        }
                    }
                    else
                    {
                        if (col == 1) MileBandLow = CellValue.Trim();
                        else if (col == 2) MileBandHigh = CellValue.Trim();
                        else if (col == 3) AgeMin = CellValue.Trim();
                        else if (col == 4) AgeMax = CellValue.Trim();
                        else if (col == 5) TermMonth = CellValue.Trim();
                        else if (col == 6) TermMile = CellValue.Trim();
                    }
                    range.Interior.Color = Microsoft.Office.Interop.Excel.XlRgbColor.rgbYellow;
                    AddData = true;
                }
            }
            wb.Save();
            wb.Close();
            app.Quit();
            Console.WriteLine("\n\nPress Enter to close the program");
            Console.ReadLine();
        }
    }
}
